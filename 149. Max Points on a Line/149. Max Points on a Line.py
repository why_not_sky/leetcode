#from itertools import groupby
import math

# Definition for a point.
class Point(object):
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b

class Line(object):
    ''' y = mx+b
    '''
    def __init__(self, p1=Point(0, 0), p2=Point(1, 1)):
        self.p1 = p1
        self.p2 = p2
        self.m = 0 if (p1.x == p2.x) else (p2.y-p1.y)/(p2.x-p1.x)
        self.b = self.p1.y - self.m * self.p1.x
        self.formula = 'y={: n}x{:+n}'.format(self.m, self.b) 

    def isEqual(self, l):
        return((self.m, self.b) == (l.m, l.b))

class Solution(object):
    def quadratic(self, z):
        #(n*n-1)/2 = z
        # (-b +- sqrt(b2-4ac))/2a
        x = (1 + math.sqrt(1+4*2*z)) / 2
        return (x)
    
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        self.size = len(points)
        if self.size <= 1: return self.size
        
        #print ([(i, j, (points[i].x, points[i].y), (points[j].x, points[j].y)) for i in range(self.size) for j in range(self.size) if i < j])
        #print ([(i, j, Line(points[i], points[j]).formula) for i in range(self.size) for j in range(self.size) if i < j])
        
        #self.matrix = [[0 for x in range(self.size)] for y in range(self.size)]
        lines = [Line(points[x], points[y]).formula for x in range(self.size) for y in range(self.size) if x < y]
        
        print (lines)
        #{x:lines.count(x) for x in set(lines)}
        #cn/2: combination
        lineCount = lines.count(max(lines, key=lines.count))
        print (lineCount)
        return int(self.quadratic(lineCount))

    def occurrence(a):
        occurrence, num_times = 0, 0
        for key, values in groupby(a, lambda x : x):
            val = len(list(values))
            if val >= occurrence:
                occurrence, num_times =  key, val
        return occurrence, num_times            

def test_line():
    l1 = Line(Point(0, 0), Point(1, 1))
    l2 = Line(Point(2, 2), Point(3, 3))

    print (l1.formula, l2.formula, l1.isEqual(l2))

test_line()

plist = [Point(0, 0), Point(1, 1), Point(1, 3), Point(2, 2)]
print (Solution().maxPoints(plist))

