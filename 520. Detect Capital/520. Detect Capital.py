class Solution(object):
    def detectCapitalUse(self, word):
        """
        :type word: str
        :rtype: bool
        """
        #if (word == word.upper()) : return True
        #if (word[1:] == word[1:].lower()): return True
        #return (False)

        return (word == word.upper() or word[1:] == word[1:].lower())

s = Solution()
print (s.detectCapitalUse('USA'))
print (s.detectCapitalUse('leetcode'))

print (s.detectCapitalUse('Usa'))
print (s.detectCapitalUse('usA'))
print (s.detectCapitalUse('u'))
