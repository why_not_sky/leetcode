create table Person(Id int, Email varchar(128));

insert into Person values (1, 'john@example.com');
insert into Person values (2, 'bob@example.com');
insert into Person values (3, 'john@example.com');

delete from Person 
where Person.Id in (
select max(Id) as Id
from Person as P
group by Email
having count(Id) > 1
    ) ;

delete p1
from Person p1,
     Person p2
where p1.Email = p2.Email
      and p1.Id > p2.Id

select * from Person;
